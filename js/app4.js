function cargarDatos() {
    const http = new XMLHttpRequest();
    const url = "https://jsonplaceholder.typicode.com/users";

    http.onreadystatechange = function () {
        if (this.status == 200 && this.readyState == 4) {
            let res = document.getElementById('lista');
            const jsonData = JSON.parse(this.responseText);

            for (const datos of jsonData) {
                res.innerHTML += `<tr>
                    <td class="columna1">${datos.id}</td>
                    <td class="columna2">${datos.name}</td>
                    <td class="columna3">${datos.username}</td>
                    <td class="columna4">${datos.email}</td>
                    <td class="columna5">${datos.address.street}, ${datos.address.suite}, ${datos.address.city}, ${datos.address.zipcode}</td>
                    <td class="columna6">${datos.phone}</td>
                    <td class="columna7">${datos.website}</td>
                    <td class="columna8">${datos.company.name}, ${datos.company.catchPhrase}, ${datos.company.bs}</td>
                </tr>`;
            }
        }
    };

    http.open('GET', url, true);
    http.send();
}

function buscarPorId() {
    const idInput = document.getElementById('idInput').value;
    const http = new XMLHttpRequest();
    const url = `https://jsonplaceholder.typicode.com/users/${idInput}`;

    if (idInput === '') {
        alert('Por favor, ingrese un ID antes de realizar la búsqueda.');
        return;
    }

    http.onreadystatechange = function () {
        if (this.readyState == 4) {
            let res = document.getElementById('lista');
            res.innerHTML = ""; // Limpiar contenido anterior

            if (this.status == 200) {
                const userData = JSON.parse(this.responseText);

                res.innerHTML += `<tr>
                    <td class="columna1">${userData.id}</td>
                    <td class="columna2">${userData.name}</td>
                    <td class="columna3">${userData.username}</td>
                    <td class="columna4">${userData.email}</td>
                    <td class="columna5">${userData.address.street}, ${userData.address.suite}, ${userData.address.city}, ${userData.address.zipcode}</td>
                    <td class="columna6">${userData.phone}</td>
                    <td class="columna7">${userData.website}</td>
                    <td class="columna8">${userData.company.name}, ${userData.company.catchPhrase}, ${userData.company.bs}</td>
                </tr>`;
            } else {
                res.innerHTML = `<tr><td colspan="8">No se encontró el registro con el ID especificado</td></tr>`;
            }
        }
    };

    http.open('GET', url, true);
    http.send();
}


document.getElementById("btnCargar").addEventListener('click', cargarDatos);
document.getElementById("btnLimpiar").addEventListener('click', function () {
    let res = document.getElementById('lista');
    res.innerHTML = "";
});

document.getElementById("btnBuscar").addEventListener('click', buscarPorId);
