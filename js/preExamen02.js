let cargandoRazas = false; 

function cargarRazas() {
    const razasCaninasSelect = document.getElementById("razasCaninasSelect");
    const cargarRazasBtn = document.getElementById("cargarRazas");

    if (!cargandoRazas) {
        cargandoRazas = true;

        const apiUrl = "https://dog.ceo/api/breeds/list";

        axios.get(apiUrl)
            .then(response => {
                const data = response.data;
                razasCaninasSelect.innerHTML = "<option value='' disabled selected>Selecciona una raza</option>";

                data.message.forEach(raza => {
                    const option = document.createElement("option");
                    option.value = raza;
                    option.textContent = raza;
                    razasCaninasSelect.appendChild(option);
                });
            })
            .catch(error => {
                console.error("Error al obtener las razas caninas:", error);
                razasCaninasSelect.innerHTML = "<option value='' disabled selected>Error al cargar las razas caninas</option>";
            })
            .finally(() => {
                cargarRazasBtn.disabled = true;
                cargandoRazas = false;
            });
    }
}

function verImagen() {
    const razasCaninasSelect = document.getElementById("razasCaninasSelect");
    const imagenPerro = document.getElementById("imagenPerro");

    const razaSeleccionada = razasCaninasSelect.value;

    if (razaSeleccionada) {
        const apiUrl = `https://dog.ceo/api/breed/${razaSeleccionada}/images/random`;

        axios.get(apiUrl)
            .then(response => {
                const data = response.data;
                imagenPerro.src = data.message;
            })
            .catch(error => {
                console.error("Error al obtener la imagen:", error);
            });
    } else {
        alert("Por favor, selecciona una raza antes de ver la imagen.");
    }
}