const llamandoFetch = async () => {
    const url = "https://jsonplaceholder.typicode.com/albums";
    try {
        const respuesta = await fetch(url);
        if (!respuesta.ok) {
            throw new Error('La respuesta no es exitosa');
        }
        const data = await respuesta.json();
        mostrarDatos(data);
    } catch (error) {
        const lblError = document.getElementById('lblError');
        lblError.textContent = "Surgió un error: " + error.message;
    }
};

const crearTabla = () => {
    const respuesta = document.getElementById('respuesta');
    respuesta.innerHTML = ''; // Limpiar contenido anterior

    const tabla = document.createElement('table');
    tabla.id = 'tablaResultados';

    const headers = ['User ID', 'ID', 'Title'];
    const thead = document.createElement('thead');
    const headerRow = document.createElement('tr');

    headers.forEach(headerText => {
        const th = document.createElement('th');
        th.textContent = headerText;
        headerRow.appendChild(th);
    });

    thead.appendChild(headerRow);
    tabla.appendChild(thead);

    const tbody = document.createElement('tbody');
    tbody.id = 'tablaBody';
    tabla.appendChild(tbody);

    respuesta.appendChild(tabla);
};

const limpiarTabla = () => {
    const tablaBody = document.getElementById('tablaBody');
    if (tablaBody) {
        tablaBody.innerHTML = '';
    }
};

const mostrarDatos = (data) => {
    if (!document.getElementById('tablaResultados')) {
        crearTabla();
    }

    limpiarTabla();

    const tablaBody = document.getElementById('tablaBody');
    
    if (data.length === 0) {
        const mensajeBusqueda = document.getElementById('mensajeBusqueda');
        mensajeBusqueda.textContent = "No hay información disponible.";
        return;
    }

    for (let item of data) {
        const row = tablaBody.insertRow();
        row.insertCell(0).textContent = item.userId;
        row.insertCell(1).textContent = item.id;
        row.insertCell(2).textContent = item.title;
    }
};

const buscarPorId = async () => {
    const idInput = document.getElementById('idInput').value;
    
    if (!idInput) {
        return;
    }

    const url = "https://jsonplaceholder.typicode.com/albums/" + idInput;

    try {
        // Limpiar mensaje de error
        const mensajeBusqueda = document.getElementById('mensajeBusqueda');
        mensajeBusqueda.textContent = '';

        const respuesta = await fetch(url);

        if (!respuesta.ok) {
            throw new Error('No se encontró el registro con el ID especificado');
        }

        const data = await respuesta.json();

        if (!document.getElementById('tablaResultados')) {
            crearTabla();
        }

        limpiarTabla();

        const tablaBody = document.getElementById('tablaBody');
        const row = tablaBody.insertRow();
        row.insertCell(0).textContent = data.userId;
        row.insertCell(1).textContent = data.id;
        row.insertCell(2).textContent = data.title;
    } catch (error) {
        const mensajeBusqueda = document.getElementById('mensajeBusqueda');
        mensajeBusqueda.textContent = error.message;
    }
};

document.getElementById('idInput').addEventListener('keypress', function (event) {
    if (event.key === 'Enter') {
        limpiarTabla();
        if (document.activeElement === this) {
            buscarPorId();
        } else {
            llamandoFetch();
        }
    }
});

document.getElementById('btnCargar').addEventListener('click', function () {
    limpiarTabla();
    llamandoFetch();
});

document.getElementById('btnBuscar').addEventListener('click', function () {
    limpiarTabla();
    buscarPorId();
});

document.getElementById('btnLimpiar').addEventListener('click', function () {
    limpiarTabla();
});
