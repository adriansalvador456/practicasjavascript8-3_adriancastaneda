function cargarDatos() {
    const http = new XMLHttpRequest();
    const url = "https://jsonplaceholder.typicode.com/users";

    // realizar función de respuesta de petición 
    http.onreadystatechange = function () {
        // validar respuesta
        if (this.status == 200 && this.readyState == 4) {
            let res = document.getElementById('lista');
            const jsonData = JSON.parse(this.responseText);

            for (const datos of jsonData) {
                res.innerHTML += `<tr>
                    <td class="columna1">${datos.id}</td>
                    <td class="columna2">${datos.name}</td>
                    <td class="columna3">${datos.username}</td>
                    <td class="columna4">${datos.email}</td>
                    <td class="columna5">${datos.address.street}, ${datos.address.suite}, ${datos.address.city}, ${datos.address.zipcode}</td>
                    <td class="columna6">${datos.phone}</td>
                    <td class="columna7">${datos.website}</td>
                    <td class="columna8">${datos.company.name}, ${datos.company.catchPhrase}, ${datos.company.bs}</td>
                </tr>`;
            }
        }
    };

    http.open('GET', url, true);
    http.send();
}

// Codificar los botones
document.getElementById("btnCargar").addEventListener('click', cargarDatos);
document.getElementById("btnLimpiar").addEventListener('click', function () {
    let res = document.getElementById('lista');
    res.innerHTML = "";
});
