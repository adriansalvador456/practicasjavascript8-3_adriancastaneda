// pre01.js
async function buscarPorId() {
    const idInput = document.getElementById('id').value;
    const url = 'https://jsonplaceholder.typicode.com/users';

    try {
        const response = await axios.get(url);
        const userData = response.data.find(user => user.id == idInput);

        if (userData) {
            document.getElementById('name').value = userData.name || '';
            document.getElementById('userName').value = userData.username || '';
            document.getElementById('email').value = userData.email || '';
            
            // Domicilio
            document.getElementById('street').value = userData.address?.street || '';
            document.getElementById('suite').value = userData.address?.suite || '';
            document.getElementById('city').value = userData.address?.city || '';
        } else {
            alert('No se encontró el usuario con el ID especificado');
        }
    } catch (error) {
        console.error('Error al realizar la petición:', error);
        alert('Error al obtener datos del usuario');
    }
}
function LimpiarCampos() {
    document.getElementById('id').value = '';
    document.getElementById('name').value = '';
    document.getElementById('userName').value = '';
    document.getElementById('email').value = '';
    document.getElementById('street').value = '';
    document.getElementById('suite').value = '';
    document.getElementById('city').value = '';
}
